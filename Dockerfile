FROM 106.75.251.56/daocloud/java:8-jre
MAINTAINER Alexander Lukyanchikov <sqshq@sqshq.com>
ADD ./target/restDemo-0.0.1-SNAPSHOT.jar /app/
CMD ["java", "-jar", "/app/restDemo-0.0.1-SNAPSHOT.jar"]


EXPOSE 8080
